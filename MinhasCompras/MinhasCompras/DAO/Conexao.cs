﻿using MinhasCompras.Modelo;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MinhasCompras.DAO
{
    class Conexao
    {
        /*
         * Singleton
         */
         private Conexao()
        {

        }

        private static Conexao instance;
        public static Conexao Instance {
            get
            {
                if (instance == null)
                {
                    instance = new Conexao();
                }
                return instance;
            }
        }

        /*
         * Singleton - Fim
         */


        private SQLiteConnection conn;

        public SQLiteConnection Conn
        {
            get
            {
                if (this.conn == null)
                {
                    string caminho = Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                        "minhascompras.db3"
                        );

                    this.conn = new SQLiteConnection(caminho);
                    this.conn.CreateTable<Produto>();
                }

                return this.conn;
            }
        }

    }
}
