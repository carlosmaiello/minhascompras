﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MinhasCompras.Erros;
using MinhasCompras.Modelo;

namespace MinhasCompras.DAO
{

    /**
     * Lembrar do CRUD
     */
    public class ProdutoDAO
    {
        public void inserir(Produto produto)
        {
            if (produto.Nome == null || produto.Nome == "")
            {
                throw new ValidacaoException("O nome não foi informado");
            }

            if (produto.Qtde < 1)
            {
                throw new ValidacaoException("A quantidade de ser maior do que zero");
            }

            produto.DataAdicao = DateTime.Now;
            produto.IsComprado = false;

            Conexao.Instance.Conn.Insert(produto);
        }

        public void alterar(Produto produto)
        {
            if (produto.Id < 1)
            {
                throw new Exception("Produto inválido!");
            }

            if (produto.Nome == null || produto.Nome == "")
            {
                throw new ValidacaoException("O nome não foi informado");
            }

            if (produto.Qtde < 1)
            {
                throw new ValidacaoException("A quantidade de ser maior do que zero");
            }

            Conexao.Instance.Conn.Update(produto);
        }

        public void remover(Produto produto)
        {
            throw new NotImplementedException();
        }

        public Produto consultar(int id)
        {
            throw new NotImplementedException();
        }

        public List<Produto> consultarComprados()
        {
            // Estudar sobre o LINQ
            var tabela = Conexao.Instance.Conn.Table<Produto>();
            return (from p in tabela
                    where p.IsComprado == true
                    orderby p.DataCompra descending
                    select p).ToList();
        }

        public List<Produto> consultarNaoComprados()
        {
            var tabela = Conexao.Instance.Conn.Table<Produto>();
            return (from p in tabela
                    where p.IsComprado == false
                   orderby p.DataAdicao descending
                   select p).ToList();
        }


    }
}
