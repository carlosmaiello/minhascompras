﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MinhasCompras.Modelo
{
    public class Produto
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Unique]
        public string Nome {get; set;}
        public int Qtde { get; set; }
        public bool IsComprado { get; set; }
        public DateTime DataAdicao { get; set; }
        public DateTime DataCompra { get; set; }
    }
}
