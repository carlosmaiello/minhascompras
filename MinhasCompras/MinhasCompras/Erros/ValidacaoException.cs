﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinhasCompras.Erros
{
    public class ValidacaoException : Exception
    {
        public ValidacaoException(string message) : base(message)
        {
        }

    }
}
