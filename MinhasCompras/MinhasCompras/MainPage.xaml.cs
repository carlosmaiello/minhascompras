﻿using MinhasCompras.DAO;
using MinhasCompras.Erros;
using MinhasCompras.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MinhasCompras
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            var dao = new ProdutoDAO();
            this.ComprarListView.ItemsSource = dao.consultarNaoComprados();
            this.CompradoListView.ItemsSource = dao.consultarComprados();
        }

        public void AdicionarClicked(object sender, EventArgs args)
        {
            var p = new Produto()
            {
                Nome = this.ProdutoEntry.Text,
                Qtde = Convert.ToInt32(this.QtdeEntry.Text)
            };

            var dao = new ProdutoDAO();
            try
            {
                dao.inserir(p);
                this.ProdutoEntry.Text = "";
                this.QtdeEntry.Text = "";
                this.ComprarListView.ItemsSource = dao.consultarNaoComprados();
            }
            catch (ValidacaoException vex)
            {
                DisplayAlert("Validação", vex.Message, "Fechar");
            }

        }

    }
}
